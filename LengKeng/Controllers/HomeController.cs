﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LengKeng.Models;
using PagedList;
using PagedList.Mvc;
using LengKeng.code;

namespace LengKeng.Controllers
{
    public class HomeController : Controller
    {
        
        private CetagoryModelProcess db = new CetagoryModelProcess();

        public ActionResult Index(string id1, string id2, int? page)
        {
            //TokenSession.setProductSesstion();
            

            string I =  DateTime.Now.ToString("d/MM/yyyy");
            if (id1 == null || id2 == null)
            {
                id1 = "1";
                id2 = "Ca";
            }

            ViewBag.id1 = id1;
            ViewBag.id2 = id2;
            ViewData["listDepartment"] = db.getListDepartment();
            int pageNumber = (page ?? 1);

            ViewData["listProduct"] = db.getListProduct(id1, id2).ToPagedList(pageNumber, 10);
            ViewBag.viewtitle = db.getTitel(id1, id2);
            return View("Index");
            
        }

        public ActionResult getInformation()
        {
            return View("Information");
        }

        public ActionResult Search(string txtsearch, int? page)
        {
            try
            {

                ViewBag.viewtitle = "Kết quả tìm kiếm";
                //if(keyword == null || keyword.Equals(string.Empty))

                ViewData["listDepartment"] = db.getListDepartment();
            int pageNumber = (page ?? 1);

            ViewData["listProduct"] = db.getProductsByName(txtsearch).ToPagedList(pageNumber, 10);;
            ViewBag.txtsearch = txtsearch;

                return View("Search");
            }
            catch
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}
