﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LengKeng.Models;
using LengKeng.code;
using LengKeng.Code;
using System.Net.Mail;

namespace LengKeng.Controllers
{
    public class AccountsController : Controller
    {
        //
        // GET: /Accounts/
        private AccountModelProcess AccM = new AccountModelProcess();
        private ShoppingCart SCart = new ShoppingCart();

        

        [HttpGet]
        public ActionResult LoginAccount()
        {
            return View("Login", new LoginModel());
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                if (user.isSocial == 0)
                {
                    return View("ChangePassword");
                }
                
            }
            return RedirectToAction("Index", "Home");
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                UserSession user = SessionHelper.getSesstion();
                if (user != null)
                {
                    if (user.isSocial == 0)
                    {
                        if (AccM.changePassword(model))
                        {
                            return View("ChangePasswordSuccess");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Mật khẩu không đúng");
                            return View("ChangePassword", model);
                        }
                    }

                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Vui lòng điền đủ thông tin");
                return View("ChangePassword", model);
            }
        }

        [HttpGet]
        public ActionResult RecoverPassword()
        {
            return View("RecoverPassword");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult RecoverPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                
                        if (AccM.resetPassword(model))
                        {
                            return View("RecoverPasswordSuccess");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Tên tài khoản không tồn tại");
                            return View("RecoverPassword", model);
                        }
                
            }
            else
            {
                ModelState.AddModelError("", "Vui lòng điền đủ thông tin");
                return View("RecoverPassword", model);
            }
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginAccount(LoginModel model)
        {
            if (AccM.login(model.UserName, model.Password) && ModelState.IsValid)
            {
                SessionHelper.setSesstion(new UserSession() { UserName = model.UserName, Admin = AccM.isAdmin(model.UserName), isSocial = 0 });
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Tên tài khoàn trùng");
                return View("Login", new LoginModel());
            }

        }

        [HttpPost]
        public ActionResult LoginBySocial(string email)
        {
            AccM.loginBySocial(email);
            return Json("/Home/Index", JsonRequestBehavior.AllowGet);
        }



        public ActionResult detailAccount()
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                ViewData["acc"] = AccM.getAccount(user.UserName);
                return View("DetailAccount");
            }
            return RedirectToAction("Index", "Home");
        }


        [HttpPost]
        public ActionResult updateAccount()
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                ProfileUserModel model = AccM.getAccount(user.UserName);

                
                return View("UpdateAccount", model);
            }
            return RedirectToAction("Index", "Home");



           
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult processUpdateAccount(ProfileUserModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AccM.updateAccount(model);
                    return RedirectToAction("detailAccount");
                }
                else
                {
                    ModelState.AddModelError("fail1", "Vui lòng điền đầy đủ thông tin");
                    return View("UpdateAccount", model);
                }
            }
            catch
            {
                ModelState.AddModelError("fail2", "Đã có lỗi, cập nhật không thành công");
                return View("UpdateAccount", model);
            }

        }


        public ActionResult Logout()
        {
            SessionHelper.setSesstion(null);
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult registerAccount()
        {
            return View("RegisterAccount", new Models.ProfileUserModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult registerAccount(ProfileUserModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (AccM.addAccount(model))
                    {
                        UserSession user = SessionHelper.getSesstion();
                        if (user == null)
                        {
                            SessionHelper.setSesstion(new UserSession() { UserName = model.username, Admin = 0 });
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            if (user.Admin == 0)
                            {
                                SessionHelper.setSesstion(new UserSession() { UserName = model.username, Admin = 0 });
                                return RedirectToAction("Index", "Home");
                            }
                            else
                            {
                                return RedirectToAction("manageAccounts");
                            }
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }

                   
                       
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Vui lòng điền đầy đủ thông tin");
                    return View(model);
                }
            }
            catch
            {
                ModelState.AddModelError(string.Empty, "Đã có lỗi, tạo tài khoản không thành công");
                return View(model);
            }
        }


        //[HttpGet]
        //public ActionResult manageAccounts()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            ViewData["Listaccounts"] = AccM.getListAccount();
        //            return View("ManageAccounts");
        //        }

        //    }
        //    return RedirectToAction("Index", "Home");
        //}
        //private LENGKENG_ICECREAM_DBEntities de = new LENGKENG_ICECREAM_DBEntities();

        //[HttpGet]
        //public ActionResult deleteAccount(string id)
        //{

        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1 && user.UserName != id)
        //        {
        //            AccM.deleteAccount(id);

        //        }

        //        return RedirectToAction("manageAccounts");
        //    }
        //    return RedirectToAction("Index", "Home");
            
        //}

        [HttpGet]
        public ActionResult sendSupport()
        {
            return View("SupportUser", new SupportModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult sendSupport(SupportModel model)
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                model.USERNAME = user.UserName;
                model.SDATECREATE = DateTime.Now;
                if (ModelState.IsValid)
                {
                    AccM.addSupport(model);
                    ModelState.AddModelError(string.Empty, "Cám ơn bạn đã gửi thư đóng góp");
                    return View("SupportSucccess");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

                    return View("SupportUser",model);
                }
            }
            else
            {
                return RedirectToAction("LoginAccount");
            }
            
        }

        public ActionResult addShopping(string id)
        {
            SCart.addListShoppingCart(id);
            //if(ShoppingHelper.getSesstion() == null)
            return Json(SCart.getAmount(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getCartView()
        { 
            return View("ShoppingCart");
        }

        public PartialViewResult Cart()
        {
            ViewData["listCart"] = SCart.getListProduct();
            ViewData["listAmonut"] = SCart.getListAmount();
            ViewData["Total"] = SCart.getTotal();
            return PartialView("cart");
        }

        public ActionResult deleteProductFromCart(string id)
        {
            SCart.RemoveProducts(id);

            return Cart();
        }

        public ActionResult updateCartItem(int id, int quantity)
        {
            SCart.updateCart(id, quantity);
            return Cart();
        }


        public ActionResult payCart()
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                SCart.pay(user.UserName);
                SCart.clear();
                return View("PaySuccess");
            }
            else
            {
                return RedirectToAction("LoginAccount"); ;
            }
            
        }

        public ActionResult historyCart()
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {

                ViewData["listBill"] = SCart.getListBill(user.UserName);
                   

                return View("HistoryCart");
            }
            else
            {
                return RedirectToAction("LoginAccount"); ;
            }
        }

        public ActionResult detailHistoryBill(string id)
        {
            UserSession user = SessionHelper.getSesstion();
            if (user != null)
            {
                
                ViewData["bill"] = SCart.getBill(id);
                    

                return View("DetailHistoryCart");
            }
            else
            {
                return RedirectToAction("LoginAccount"); ;
            }
        }


        //public ActionResult getListBill()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            LENGKENG_ICECREAM_DBEntities db = new LENGKENG_ICECREAM_DBEntities();

        //            ViewData["listbill"] = db.BILL.ToList();

        //            return View("ManageBill");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //public ActionResult confirmPay(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        LENGKENG_ICECREAM_DBEntities db = new LENGKENG_ICECREAM_DBEntities();
        //        int i = Int32.Parse(id);
        //        BILL bill = db.BILL.Single(t => t.BID == i);
        //        bill.BSTATUS = "Đã thanh toán";
        //        db.SaveChanges();
        //        return RedirectToAction("getListBill");
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}


        //public void SendEmail(string toEmail, String subject, String body)
        //{

        //    var message = new MailMessage("quan.nguyen@vnresource.net", toEmail)
        //    {
        //        Subject = subject,
        //        Body = body

        //    };
        //    message.IsBodyHtml = true;
        //    var client = new SmtpClient();
        //    client.EnableSsl = true;
        //    client.Send(message);
        //}

        //[AllowAnonymous]
        //public ActionResult ResetPassword()
        //{
        //    return View();
        //}

        //[AllowAnonymous]
        //[HttpPost]
        //public ActionResult ResetPassword(ResetPasswordModel model)
        //{
        //    string emailAddress = GetEmail(model.UserName);
        //    if (!string.IsNullOrEmpty(emailAddress))
        //    {
        //        string confirmationToken =
        //            WebSecurity.GeneratePasswordResetToken(model.UserName);

        //        String bodyMsg = "http://localhost:29105/Account/ResetPasswordConfirmation/" + confirmationToken;
        //        String subjectMsg = "Xác nhận reset mật khẩu Shopxachtay.us";

        //        SendEmail(emailAddress, subjectMsg, bodyMsg);
        //        return RedirectToAction("ResetPwStepTwo");
        //    }
        //    return RedirectToAction("InvalidUserName");
        //}

        //private string GetEmail(string username)
        //{
        //    ShopXachTayBLL.ShopXachTayEntities db = new ShopXachTayBLL.ShopXachTayEntities();
        //    ShopXachTayBLL.UserProfile user = db.UserProfiles.Where(m => m.UserName == username).ToList().ElementAt(0);
        //    ShopXachTayBLL.TaiKhoan taiKhoan = db.TaiKhoans.Where(m => m.UserID == user.UserId).ToList().ElementAt(0);

        //    return taiKhoan.Email;
        //}
    }
}
