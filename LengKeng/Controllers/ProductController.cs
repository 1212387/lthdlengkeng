﻿using LengKeng.code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LengKeng.Models;
using LengKeng.Code;

namespace LengKeng.Controllers
{
    public class ProductController : Controller
    {
        private CetagoryModelProcess db = new CetagoryModelProcess();
        //
        // GET: /Product/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult detailProduct(string id)
        {
            ViewData["listDepartment"] = db.getListDepartment();
            ViewData["product"] = db.getProduct(id);
            db.tangView(id);

            return View("DetailProduct", new CommentModel());
        }


        [HttpPost]
        public ActionResult addComent(string pid, string username, string commentContent)
        {

            CommentModel temp = new CommentModel();
            temp.CMCONTENT = commentContent;
            temp.IDPRODUCT = Int32.Parse(pid);
            temp.USERNAME = username;

            db.writeComment(temp);
                return Comment(pid);
            

        }


        //public ActionResult mangeDetailProduct()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {


        //            ViewData["listProduct"] = db.getListProduct();

        //            return View("ManageListProduct");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //[HttpGet]
        //public ActionResult addProduct()
        //{
        //    return View("AddProduct", new ProductModel());
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult addProduct(ProductModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        int id = db.addProduct(model);

        //        return RedirectToAction("detailProduct/" + id);
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("AddProduct", model);
        //    }
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult manageUpdateProduct(ProductModel model)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        db.updateProduct(model);

        //        return RedirectToAction("detailProduct/" + model.PID);
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("ManageUpdateProduct", model);
        //    }


        //}

        //[HttpGet]
        //public ActionResult manageUpdateProduct(string id)
        //{
        //    ProductModel model = db.getProductModel(id);
        //    return View("ManageUpdateProduct", model);
        //}

        //public ActionResult mangeCategory()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {

        //            ViewData["listCategory"] = db.getListCategory();
        //            return View("ManageCatagory");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //public ActionResult deleteCategory(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            db.deleteCategory(id);
        //            return RedirectToAction("mangeCategory");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //[HttpGet]
        //public ActionResult updateCategory(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            CategoryModel model = db.getCategory(id);
        //            return View("UpdateCategory", model);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult updateCategory(CategoryModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.updateCategory(model);
        //        return RedirectToAction("mangeCategory");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("UpdateCategory", model);
        //    }

        //}

        //[HttpGet]
        //public ActionResult addCategory()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            return View("AddCategory", new CategoryModel());
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult addCategory(CategoryModel model)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        db.addCategory(model);
        //        return RedirectToAction("mangeCategory");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("AddCategory", model);
        //    }

        //}

        //public ActionResult mangeDepartment()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {

        //            ViewData["listDepartment"] = db.getListDepartment();
        //            return View("ManageDepartment");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //[HttpGet]
        //public ActionResult addDepartment()
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            return View("AddDepartment", new DepartmentCategory());
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult addDepartment(DepartmentCategory model)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        db.addDepartment(model);
        //        return RedirectToAction("mangeDepartment");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("AddDepartment", model);
        //    }
        //}

        //[HttpGet]
        //public ActionResult updateDepartment(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {
        //            DepartmentCategory model = db.getDepartment(id);
        //            return View("UpdateDepartment", model);
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult updateDepartment(DepartmentCategory model)
        //{
        //    if (ModelState.IsValid)
        //    {

        //        db.updateDepartment(model);
        //        return RedirectToAction("mangeDepartment");
        //    }
        //    else
        //    {
        //        ModelState.AddModelError(string.Empty, "Vui lòng nhập đầy đủ thông tin");

        //        return View("UpdateDepartment", model);
        //    }
        //}


        //public ActionResult deleteDepartment(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {


        //            db.deleteDepartment(id);

        //            return RedirectToAction("mangeDepartment");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        //public ActionResult deleteProduct(string id)
        //{
        //    UserSession user = SessionHelper.getSesstion();
        //    if (user != null)
        //    {
        //        if (user.Admin == 1)
        //        {


        //            db.deleteProduct(id);

        //            return RedirectToAction("mangeDetailProduct");
        //        }
        //        else
        //        {
        //            return RedirectToAction("Index", "Home");
        //        }
        //    }
        //    else
        //    {
        //        return RedirectToAction("LoginAccount"); ;
        //    }
        //}

        public PartialViewResult Comment(string id)
        {
            ViewData["listComment"] = db.getListCommentByIdProduct(id);
            ViewBag.PID = id;
            return PartialView("Comment");
        }
    }
}
