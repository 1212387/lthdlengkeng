﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LengKeng.Models
{
    public class ResetPasswordModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

    }

    public class ChangePasswordModel
    {

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Old password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu mới")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập xác nhận mật khẩu")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập!")]
        [Display(Name = "Username: ")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập mật khẩu!")]
        [MaxLength(50, ErrorMessage = "Độ dài mật khẩu tối đa 30 kí tự!")]
        [MinLength(6, ErrorMessage = "Độ dài mật khẩu tối thiểu 4 kí tự!")]
        [Display(Name = "Password: ")]
        public string Password { get; set; }
        public string rememberMe { get; set; }

    }

    public class ProfileUserModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên đăng nhập")]
        public string username { get; set; }


        [Required(ErrorMessage = "Vui lòng nhập mật khẩu")]
        [MaxLength(50, ErrorMessage = "Độ dài mật khẩu tối đa 20 kí tự")]
        [MinLength(6, ErrorMessage = "Độ dài mật khẩu tối thiểu 6 kí tự")]
        [Display(Name = "Mật khẩu")]
        public string password { get; set; }


        [Compare("password", ErrorMessage = "Mật khẩu nhập lại không trùng khớp")]
        [MaxLength(50, ErrorMessage = "Độ dài mật khẩu tối đa 20 kí tự")]
        [MinLength(6, ErrorMessage = "Độ dài mật khẩu tối thiểu 6 kí tự")]
        [Display(Name = "Nhập lại mật khẩu")]
        [Required(ErrorMessage = "Vui lòng nhập lại mật khẩu")]
        public string passwordRetyped { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn quyền")]
        public int admin { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        public string fullname { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn giới tính")]
        public string sex { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ email")]
        [EmailAddress(ErrorMessage = "Định dạng email sai")]
        public string email { get; set; }


        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        [RegularExpression(@"((09)\d{8})|((01)\d{9})", ErrorMessage = "Số điện thoại phải là số, bắt đầu bằng 09 (8 chữ số tiếp theo) hoặc bắt đầu bằng 01 (9 chữ số tiếp theo)")]
        public string phonenumber { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập ngày sinh")]
        public string bday { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập địa chỉ")]
        public string address { get; set; }


        public System.Web.Mvc.SelectList getSex()
        {
            List<System.Web.Mvc.SelectListItem> list = new List<System.Web.Mvc.SelectListItem>();
            list.Add(new System.Web.Mvc.SelectListItem { Value = "Nam", Text = "Nam" });
            list.Add(new System.Web.Mvc.SelectListItem { Value = "Nữ", Text = "Nữ" });
            return new System.Web.Mvc.SelectList(list, "Value", "Text", sex);
        }

        public System.Web.Mvc.SelectList getAdmin()
        {
            List<System.Web.Mvc.SelectListItem> list = new List<System.Web.Mvc.SelectListItem>();
            list.Add(new System.Web.Mvc.SelectListItem { Value = "1", Text = "Admin" });
            list.Add(new System.Web.Mvc.SelectListItem { Value = "0", Text = "User" });
            return new System.Web.Mvc.SelectList(list, "Value", "Text", admin);
        }
    }

}