﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LengKeng.Models
{
    public class CommentModel
    {

        [Required(ErrorMessage = "Vui lòng điền nội dung bình luận")]
        [DataType(DataType.Text)]
        public string CMCONTENT { get; set; }
        public int IDPRODUCT { get; set; }
        public string USERNAME { get; set; }

        
    }
}