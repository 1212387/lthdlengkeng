﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace LengKeng.Models
{
    public class DepartmentCategory
    {
        public int DID { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên department")]
        public string DNAME { get; set; }
    }
}