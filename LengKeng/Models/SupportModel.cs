﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace LengKeng.Models
{
    public class SupportModel
    {
        [Required(ErrorMessage = "Vui lòng điền tiêu đề")]
        public string STITEL { get; set; }

        [Required(ErrorMessage = "Vui lòng điền nội dung phản hồi")]
        [DataType(DataType.Text)]
        public string SCONTENT { get; set; }

        public string USERNAME { get; set; }
        public System.DateTime SDATECREATE { get; set; }
    }
}