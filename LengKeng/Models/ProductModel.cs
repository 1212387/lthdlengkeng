﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace LengKeng.Models
{
    public class ProductModel
    {

        private LENGKENG_ICECREAM_DBEntities db = new LENGKENG_ICECREAM_DBEntities();
        public int PID { get; set; }

        [Required(ErrorMessage = "Vui lòng tên sản phẩm")]
        public string PNAME { get; set; }

        [Required(ErrorMessage = "Vui lòng điền giá sản phẩm")]
        public int PCOST { get; set; }

        [Required(ErrorMessage = "Vui lòng điền mùi vị sản phẩm")]
        public string PSAVOUR { get; set; }

        [Required(ErrorMessage = "Vui lòng điền tình trạng sản phẩm")]
        public string PSTATUS { get; set; }

        [Required(ErrorMessage = "Vui lòng điền mô tả")]
        [DataType(DataType.Text)]
        public string PDESCRIPTION { get; set; }

        [Required(ErrorMessage = "Vui lòng điền đường dẫn hình ảnh")]
        public string PIMG { get; set; }


        public Nullable<int> PVIEW { get; set; }
        public string IDCATELOGY { get; set; }

        public System.Web.Mvc.SelectList getCategory()
        {
            List<CATELOGIES> listtemp = db.CATELOGIES.ToList();
            
            List<System.Web.Mvc.SelectListItem> list = new List<System.Web.Mvc.SelectListItem>();
            foreach (CATELOGIES temp in listtemp)
            {
                list.Add(new System.Web.Mvc.SelectListItem { Value = temp.CID.ToString(), Text = temp.CNAME });
            }
            
            
            return new System.Web.Mvc.SelectList(list, "Value", "Text", IDCATELOGY);
        }
    }
}