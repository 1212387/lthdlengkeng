﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace LengKeng.Models
{
    public class CategoryModel
    {
        private LENGKENG_ICECREAM_DBEntities db = new LENGKENG_ICECREAM_DBEntities();
        public int CID { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập tên category")]
        public string CNAME { get; set; }
        [Required(ErrorMessage = "Vui lòng chọn department")]
        public string IDDEPARTMENT { get; set; }

        public System.Web.Mvc.SelectList getDeparment()
        {
            List<DEPARTMENTS> listtemp = db.DEPARTMENTS.ToList();

            List<System.Web.Mvc.SelectListItem> list = new List<System.Web.Mvc.SelectListItem>();
            foreach (DEPARTMENTS temp in listtemp)
            {
                list.Add(new System.Web.Mvc.SelectListItem { Value = temp.DID.ToString(), Text = temp.DNAME });
            }


            return new System.Web.Mvc.SelectList(list, "Value", "Text", IDDEPARTMENT);
        }
    }
}