﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LengKeng.Models
{
    public class changepasswordObject
    {
        public string username { get; set; }
        public string oldpassword { get; set; }
        public string newpassword { get; set; }
    }
}