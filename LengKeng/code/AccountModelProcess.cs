﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LengKeng.Models;
using LengKeng.Code;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace LengKeng.code
{
    public class AccountModelProcess
    {
        public bool login(string username, string password)
        {
            try
            {
                ACCOUNTS temp = new ACCOUNTS();
                temp.USERNAME = username;
                temp.PASSWORD = password;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                    // New code:
                    HttpResponseMessage response = client.PostAsJsonAsync("api/AccountLogin", temp).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return true;


                    }
                    else if (response.StatusCode == (HttpStatusCode)401)
                    {
                        TokenSession.setProductSesstion();
                        return login(username, password);
                    }
                    return false;

                }
               
            }
            catch
            {
                return false;
            }
            
        }

        public int isAdmin(string username)
        {
            var tk = getAccountFromAPI(username);
            return tk.ADMIN;
        }

        private ACCOUNTS getAccountFromAPI(string username)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/getAccountByID", username).Result;
                if (response.IsSuccessStatusCode)
                {
                   return  response.Content.ReadAsAsync<ACCOUNTS>().Result;
                   

                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getAccountFromAPI(username);
                     
                }
                else
                {
                    return null;
                }

            }
        }

        public ProfileUserModel getAccount(string username)
        {
            var tk = getAccountFromAPI(username);
            ProfileUserModel temp = new ProfileUserModel();

            temp.username = tk.USERNAME;
            temp.password = tk.PASSWORD;
            temp.passwordRetyped = tk.PASSWORD;
            temp.admin = tk.ADMIN;
            temp.fullname = tk.FULLNAME;
            temp.sex = tk.SEX;
            temp.email = tk.EMAIL;
            temp.phonenumber = tk.PHONENUMBER;
            temp.bday = tk.BIRTHDAY;
            temp.address = tk.ADDRESS;
            return temp;
        }

        public void updateAccount(ProfileUserModel model)
        {
            ACCOUNTS tk = new ACCOUNTS();
            tk.USERNAME = model.username;
            tk.ADMIN = model.admin;
            tk.PASSWORD = model.password;
            tk.FULLNAME = model.fullname;
            tk.SEX = model.sex;
            tk.EMAIL = model.email;
            tk.PHONENUMBER = model.phonenumber;
            tk.BIRTHDAY = model.bday;
            tk.ADDRESS = model.address;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PutAsJsonAsync("api/Accounts", tk).Result;
               if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    updateAccount(model);
                     
                }
            }
            
        }

        public bool addAccount(ProfileUserModel model)
        {
            ACCOUNTS acc = new ACCOUNTS();
            acc.USERNAME = model.username;
            acc.PASSWORD = model.password;
            
            acc.ADMIN = model.admin;

            acc.FULLNAME = model.fullname;
            acc.BIRTHDAY = model.bday;
            acc.SEX = model.sex;
            acc.EMAIL = model.email;
            acc.PHONENUMBER = model.phonenumber;
            acc.ADDRESS = model.address;
            if (addAccountToAPI(acc) != null)
                return true;
            return false;
            
        }


        private ACCOUNTS addAccountToAPI(ACCOUNTS acc)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/Accounts", acc).Result;
                if (response.IsSuccessStatusCode)
                {
                    return response.Content.ReadAsAsync<ACCOUNTS>().Result;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return addAccountToAPI(acc);

                }
                return null;

            }
        }

    

      

        public void addSupport(SupportModel model)
        {
            SUPPORT temp = new SUPPORT();
            temp.STITEL = model.STITEL;
            temp.SCONTENT = model.SCONTENT;
            temp.SDATECREATE = model.SDATECREATE;
            temp.USERNAME = model.USERNAME;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/Supports", temp).Result;
                 if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    addSupport(model);
                     
                }

            }
        }

        internal void loginBySocial(string email)
        {
            try
            {
                var tk = getAccountFromAPI(email);
                if (tk == null)
                {
                   tk = new ACCOUNTS();
                   tk.USERNAME = email;
                   tk.PASSWORD = Guid.NewGuid().ToString("N").Substring(0, 10);

                   tk.ADMIN = 0;



                   addAccountToAPI(tk);
                }
                SessionHelper.setSesstion(new UserSession() { UserName = tk.USERNAME, Admin = tk.ADMIN, isSocial = 1 });
            }
            catch
            {
                
            }
        }

        internal bool changePassword(ChangePasswordModel model)
        {
            var user = SessionHelper.getSesstion();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                changepasswordObject body = new changepasswordObject();
                body.username = user.UserName;
                body.oldpassword = model.OldPassword;
                body.newpassword = model.NewPassword;

                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/ChangePassword", body).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return changePassword(model);

                }
                return false;

            }
        }

        internal bool resetPassword(ResetPasswordModel model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/RecoveryPassword", model.UserName).Result;
                if (response.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return resetPassword(model);

                }
                return false;

            }
            
        }
    }
}