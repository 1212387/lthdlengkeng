﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LengKeng.Code
{
    [Serializable]
    public class UserSession
    {
        public string UserName { get; set; }
        public int Admin { get; set; }
        //1 is Login by Social Login
        //0 is not
        public int isSocial { get; set; }
        
    }
}