﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LengKeng.Models;
using LengKeng.Code;

namespace LengKeng.code
{
    public class cartSession
    {
        public static void setProductSesstion(List<PRODUCTS> session)
        {
            HttpContext.Current.Session["cart"] = session;
        }

        public static List<PRODUCTS> getProductSesstion()
        {
            var session = HttpContext.Current.Session["cart"];
            if (session == null)
            {
                return null;
            }
            else
                return session as List<PRODUCTS>;
        }

        public static void setAmountSession(List<int> session)
        {
            HttpContext.Current.Session["cartAmount"] = session;
        }


        public static List<int> getAmountSession()
        {
            var session = HttpContext.Current.Session["cartAmount"];
            if (session == null)
            {
                return null;
            }
            else
                return session as List<int>;
        }
    }
}