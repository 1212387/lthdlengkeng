﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LengKeng.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace LengKeng.code
{
    public class CetagoryModelProcess
    {
       

        public List<DEPARTMENTS> getListDepartment()
        {
            List<DEPARTMENTS> listDe = new List<DEPARTMENTS>();
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/Departments").Result;
                if (response.IsSuccessStatusCode)
                {
                    var temp = response.Content.ReadAsAsync<List<DEPARTMENTS>>().Result;
                    listDe = new List<DEPARTMENTS>(temp);
                    
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getListDepartment();

                }
                else
                {
                    return listDe;
                }

                response = client.GetAsync("api/Catelogies").Result;
                if (response.IsSuccessStatusCode)
                {
                    var temp = response.Content.ReadAsAsync<List<CATELOGIES>>().Result;
                    foreach (var item in listDe)
                    {
                        foreach (var item1 in temp)
                        {
                            if (item.DID == item1.IDDEPARTMENT)
                                item.CATELOGIES.Add(item1);
                        }
                    }
                    
                    

                }
                else
                {
                    return listDe;
                }
            }



            return listDe;
        }

        //public List<CATELOGIES> getListCatelogy()
        //{
        //    return db.CATELOGIES.ToList();
        //}

        public PRODUCTS getProduct(string id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                

                // New code:
                HttpResponseMessage response = client.GetAsync("api/Products/" + id.ToString()).Result;
                if (response.IsSuccessStatusCode)
                {
                    var orders = response.Content.ReadAsAsync<PRODUCTS>().Result;

                    return orders;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getProduct(id);

                }
                else
                {
                    return new PRODUCTS();
                }
            }


        }

        //public ProductModel getProductModel(string id)
        //{
        //    PRODUCTS temp = getProduct(id);
        //    ProductModel tempModel = new ProductModel();
        //    tempModel.PID = temp.PID;
        //    tempModel.PNAME = temp.PNAME;
        //    tempModel.PCOST = temp.PCOST;
        //    tempModel.PSAVOUR = temp.PSAVOUR;
        //    tempModel.PSTATUS = temp.PSTATUS;
        //    tempModel.PDESCRIPTION = temp.PDESCRIPTION;
        //    tempModel.PIMG = temp.PIMG;
        //    tempModel.PVIEW = temp.PVIEW;
        //    tempModel.IDCATELOGY = temp.IDCATELOGY.ToString();
        //    return tempModel;
        //}

        public List<PRODUCTS> getListProduct(string id1, string id2)
        {
            List<PRODUCTS> listPro = new List<PRODUCTS>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/getProductByCateAndDepart/" + id1.ToString() + "/" + id2.ToString()).Result;
                if (response.IsSuccessStatusCode)
                {
                    listPro = response.Content.ReadAsAsync<List<PRODUCTS>>().Result;

                    return listPro;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getListProduct(id1,id2);

                }
                else
                {
                    return listPro;
                }
            }
            
            
        }

        //public List<PRODUCTS> getListProduct()
        //{
        //    return db.PRODUCTS.ToList();
        //}



        public dynamic getTitel(string id1, string id2)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/getNameCate/" + id1.ToString() + "/" + id2.ToString()).Result;
                if (response.IsSuccessStatusCode)
                {
                    return  response.Content.ReadAsAsync<string>().Result;

                    
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getTitel(id1,id2);

                }
                else
                {
                    return "";
                }
            }
        }

        public void writeComment(CommentModel model)
        {
            try
            {
                COMMENTS temp = new COMMENTS();
                temp.CMCONTENT = model.CMCONTENT;
                temp.IDPRODUCT = model.IDPRODUCT;
                temp.USERNAME = model.USERNAME;
                temp.CDATECREATE = DateTime.Now;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                    // New code:
                    HttpResponseMessage response = client.PostAsJsonAsync("api/Comments", temp).Result;
                    if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    writeComment(model);

                }
                }

                
            }
            catch
            {

            }
        }

        //public void deleteProduct(string id)
        //{
        //    try
        //    {
        //        int i = Int32.Parse(id);
        //        PRODUCTS temp = db.PRODUCTS.Single(t => t.PID == i);
        //        db.PRODUCTS.Remove(temp);
        //        db.SaveChanges();
        //    }
        //    catch
        //    {

        //    }
        //}

        //public int addProduct(ProductModel model)
        //{
        //    PRODUCTS temp = new PRODUCTS();
        //    temp.PNAME = model.PNAME;
        //    temp.PCOST = model.PCOST;
        //    temp.PSAVOUR = model.PSAVOUR;
        //    temp.PDESCRIPTION = model.PDESCRIPTION;
        //    temp.PIMG = model.PIMG;
        //    temp.PSTATUS = model.PSTATUS;
        //    temp.IDCATELOGY = Int32.Parse(model.IDCATELOGY);
        //    temp.PVIEW = 0;
        //    db.PRODUCTS.Add(temp);
        //    db.SaveChanges();
        //    return temp.PID;
        //}

        //public void updateProduct(ProductModel model)
        //{
        //    PRODUCTS temp = db.PRODUCTS.Single(t => t.PID == model.PID);
        //    temp.PNAME = model.PNAME;
        //    temp.PCOST = model.PCOST;
        //    temp.PSAVOUR = model.PSAVOUR;
        //    temp.PDESCRIPTION = model.PDESCRIPTION;
        //    temp.PIMG = model.PIMG;
        //    temp.PSTATUS = model.PSTATUS;
        //    temp.IDCATELOGY = Int32.Parse(model.IDCATELOGY);
        //    db.SaveChanges();
        //}

        internal void tangView(string id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/increaseView/" + id.ToString()).Result;
                 if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    tangView(id);

                }
            }
        }

        //internal List<CATELOGIES> getListCategory()
        //{
        //    return db.CATELOGIES.ToList();
        //}

        //internal void deleteCategory(string id)
        //{
        //    try
        //    {
        //        int i = Int32.Parse(id);
        //        CATELOGIES temp = db.CATELOGIES.Single(t => t.CID == i);
        //        db.CATELOGIES.Remove(temp);
        //        db.SaveChanges();
        //    }
        //    catch
        //    {

        //    }
        //}

        //internal void addCategory(CategoryModel model)
        //{
        //    CATELOGIES temp = new CATELOGIES();
        //    temp.CNAME = model.CNAME;
        //    temp.IDDEPARTMENT = Int32.Parse(model.IDDEPARTMENT);
        //    db.CATELOGIES.Add(temp);
        //    db.SaveChanges();
        //}

        //internal CategoryModel getCategory(string id)
        //{
        //    int i = Int32.Parse(id);
        //    CategoryModel model = new CategoryModel();
        //    CATELOGIES temp = db.CATELOGIES.Single(t => t.CID == i);
        //    model.CID = temp.CID;
        //    model.CNAME = temp.CNAME;
        //    model.IDDEPARTMENT = temp.IDDEPARTMENT.ToString();
        //    return model;
        //}

        //internal void updateCategory(CategoryModel model)
        //{
        //    CATELOGIES temp = db.CATELOGIES.Single(t => t.CID == model.CID);
        //    temp.CNAME = model.CNAME;
        //    temp.IDDEPARTMENT = Int32.Parse(model.IDDEPARTMENT);
        //    db.SaveChanges();
        //}

        //internal void updateDepartment(DepartmentCategory model)
        //{
        //    DEPARTMENTS temp = db.DEPARTMENTS.Single(t => t.DID == model.DID);
        //    temp.DNAME = model.DNAME;
        //    db.SaveChanges();
        //}

        //internal void addDepartment(DepartmentCategory model)
        //{
        //    DEPARTMENTS temp = new DEPARTMENTS();
        //    temp.DNAME = model.DNAME;
        //    db.DEPARTMENTS.Add(temp);
        //    db.SaveChanges();
        //}

        //internal DepartmentCategory getDepartment(string id)
        //{
        //    DepartmentCategory temp = new DepartmentCategory();
        //    int i = Int32.Parse(id);
        //    DEPARTMENTS de = db.DEPARTMENTS.Single(t => t.DID == i);
        //    temp.DNAME = de.DNAME;
        //    temp.DID = de.DID;
        //    return temp;
        //}

        //internal void deleteDepartment(string id)
        //{
        //    try
        //    {
        //        int i = Int32.Parse(id);
        //        DEPARTMENTS de = db.DEPARTMENTS.Single(t => t.DID == i);
        //        db.DEPARTMENTS.Remove(de);
        //        db.SaveChanges();
        //    }
        //    catch
        //    {

        //    }
        //}

        internal List<COMMENTS> getListCommentByIdProduct(string id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/getListCommentByIdProduct/" + id.ToString()).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<COMMENTS>>().Result;
                    return result;

                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getListCommentByIdProduct(id);

                }
                else
                {
                    return new List<COMMENTS>();
                }
            }
        }

        internal List<PRODUCTS> getProductsByName(string txtsearch)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/searchProducts/" + txtsearch).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<PRODUCTS>>().Result;
                    return result;

                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getProductsByName(txtsearch);

                }
                else
                {
                    return new List<PRODUCTS>();
                }
            }
        }
    }
}