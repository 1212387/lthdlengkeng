﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace LengKeng.code
{
    public class TokenSession
    {
        public static void setProductSesstion()
        {
            
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("{0}:{1}", contract.PARTNER_INFO_SELLSIDE.USERNAME, contract.PARTNER_INFO_SELLSIDE.PASSWORD))));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"), 
                new KeyValuePair<string, string>("username", "admin"),
                new KeyValuePair<string, string>("password", "123456")
            });



                // New code:
                HttpResponseMessage response = client.PostAsync("/token", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<Token>().Result;

                   HttpContext.Current.Session["token"] = result;
                }
                else
                {
                    
                }
            }


            
        }

        public static Token getProductSesstion()
        {
            var session = HttpContext.Current.Session["token"];
            if (session == null)
            {
                return new Token();
            }
            else
                return session as Token;
        }
    }
}