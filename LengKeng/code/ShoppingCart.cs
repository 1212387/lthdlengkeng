﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LengKeng.Models;
using LengKeng.Code;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
namespace LengKeng.code
{
    public class ShoppingCart
    {
        
       

        public ShoppingCart()
        {
          
            if (cartSession.getAmountSession() == null)
            {
                cartSession.setAmountSession(new List<int>());
            }
            if (cartSession.getProductSesstion() == null)
            {
                cartSession.setProductSesstion(new List<PRODUCTS>());
            }
        }




        public void addListShoppingCart(string id)
        {
            CetagoryModelProcess temp = new CetagoryModelProcess();
            List<PRODUCTS> listProduct = cartSession.getProductSesstion();
            
            listProduct.Add(temp.getProduct(id));

            cartSession.setProductSesstion(listProduct);

            processList();
        }

        private void processList()
        {

            List<PRODUCTS> listProduct = cartSession.getProductSesstion();
            List<int> listAmount = cartSession.getAmountSession();

            
            for (int i = 0; i < listProduct.Count; i++)
            {
                
                for (int j = i + 1; j < listProduct.Count; j++)
                {
                    if (listProduct[i].PID == listProduct[j].PID)
                    {
                        listProduct.RemoveAt(j);
                        listAmount[i] += 1;
                        j--;
                        
                    }
                }


            }
            if (listProduct.Count > listAmount.Count)
            {
                listAmount.Add(1);
            }
            cartSession.setProductSesstion(listProduct);
            cartSession.setAmountSession(listAmount);
            
        }

        public List<PRODUCTS> getListProduct()
        {

            return cartSession.getProductSesstion();
        }

        public List<int> getListAmount()
        {
            return cartSession.getAmountSession();
        }

        public int getAmount()
        {
            List<int> temp = cartSession.getAmountSession();
            int total = 0;
            foreach (var item in temp)
            {
                total += item;
            }
            return total;
        }


        public void pay(string username)
        {
            BILL bill = new BILL();

            bill.BDATECREATE = DateTime.Now;
            bill.USERNAME = username;
            bill.BTOTAL = getTotal();
            bill.BSTATUS = "Chưa thanh toán";


            List<PRODUCTS> listProduct = cartSession.getProductSesstion();
            List<int> listAmount = cartSession.getAmountSession();

            BILLDETAIL billdetail = null;
            for (int i = 0; i < listProduct.Count; i++ )
            {
                billdetail = new BILLDETAIL();
                billdetail.IDPRODUCT = listProduct[i].PID;
                billdetail.BID = bill.BID;
                billdetail.AMOUNT = listAmount[i];
                bill.BILLDETAIL.Add(billdetail);

            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/Bills", bill).Result;
                if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    pay(username);

                }
            }


        }

        public int getTotal()
        {
            List<PRODUCTS> listProduct = cartSession.getProductSesstion();
            List<int> listAmount = cartSession.getAmountSession();


            int total = 0;
            for (int i = 0; i < listProduct.Count; i++)
            {
                total += (listProduct[i].PCOST * listAmount[i]);
            }
                return total;
        }

        public void RemoveProducts(string id)
        {
            List<PRODUCTS> listProduct = cartSession.getProductSesstion();
            List<int> listAmount = cartSession.getAmountSession();



            listProduct.RemoveAt(Int32.Parse(id));
            listAmount.RemoveAt(Int32.Parse(id));

            cartSession.setProductSesstion(listProduct);
            cartSession.setAmountSession(listAmount);
        }

        public void clear()
        {

            cartSession.setProductSesstion(new List<PRODUCTS>());
            cartSession.setAmountSession(new List<int>());
        }

        internal void updateCart(int id, int quantity)
        {
            List<int> listAmount = cartSession.getAmountSession();
   

            listAmount[id] = quantity;
            cartSession.setAmountSession(listAmount);
        }

        internal object getBill(string id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.GetAsync("api/getDetailBillByID/" + id.ToString()).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<BILLDETAIL>>().Result;
                    return result;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getBill(id);

                }
                else
                {
                    return new List<BILLDETAIL>();
                }

            }

        }

        internal object getListBill(string p)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://ltdtwebapi.apphb.com/");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", TokenSession.getProductSesstion().access_token);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));




                // New code:
                HttpResponseMessage response = client.PostAsJsonAsync("api/getListBillByUsername",p).Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsAsync<List<BILL>>().Result;
                    return result;
                }
                else if (response.StatusCode == (HttpStatusCode)401)
                {
                    TokenSession.setProductSesstion();
                    return getListBill(p);

                }
                else
                {
                    return new List<BILL>();
                }

            }
        }
    }
}