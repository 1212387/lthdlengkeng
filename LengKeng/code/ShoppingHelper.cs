﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LengKeng.code
{
    public class ShoppingHelper
    {
        public void setSesstion(int count)
        {
            HttpContext.Current.Session["ShoppingSession"] = count;
        }

        public int getSesstion()
        {
            var session = HttpContext.Current.Session["ShoppingSession"];
            if (session == null)
            {
                return 0;
            }
            else
                return 1;
        }
    }
}