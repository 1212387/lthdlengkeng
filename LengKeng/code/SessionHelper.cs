﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LengKeng.Code
{
    public class SessionHelper
    {
        public static void setSesstion(UserSession session)
        {
            HttpContext.Current.Session["loginSession"] = session;
        }

        public static UserSession getSesstion()
        {
            var session = HttpContext.Current.Session["loginSession"];
            if (session == null)
            {
                return null;
            }
            else
                return session as UserSession;
        }
    }
}