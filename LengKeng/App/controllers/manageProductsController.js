﻿'use strict';

app.controller('manageProductsController', ['$scope', 'manageProductsService', '$location', '$http', function ($scope, productsService, $location, $http) {
    $scope.message = "Quản lý sản phẩm";

    $scope.listProducts = {
        PID: "",
        PNAME: "",
        PCOST: "",
        PSAVOUR: "",
        PSTATUS: "",
        PDESCRIPTION: "",
        PIMG: "",
        PVIEW: "",
        IDCATEGORY: "",
        BILLDETAIL: "",
        CATEGORIES: "",
        COMMENTS: ""
    };

    productsService.getProducts($scope);

    $scope.delproduct = function (PID) {
        var r = confirm("Bạn có thật sự muốn xóa?");
        if (r == true) {
            return $http({
                method: "DELETE",
                url: "http://ltdtwebapi.apphb.com/api/Products/" + PID,
                headers: { 'Content-Type': 'application/json' }
            }).success(function (data) {
                console.log(data);
                alert("Xóa thành công!");
                $location.path('/manageproducts');
            }).error(function (data) {
                console.log(data);
                alert("Xóa thất bại!");
            });
        }
    };
}])

app.service('manageProductsService', ['$http', function ($http) {
    this.getProducts = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Products",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listProducts = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('detailProductController', ['$scope', 'detailProductService', '$routeParams', 'detailCategoryService', function ($scope, singleService, $routeParams, categoryService) {
    $scope.message = "Chi tiết sản phẩm";

    $scope.product = {
        PID: "",
        PNAME: "",
        PCOST: "",
        PSAVOUR: "",
        PSTATUS: "",
        PDESCRIPTION: "",
        PIMG: "",
        PVIEW: "",
        CNAME: ""
    };

    $scope.PID = $routeParams.PID;

    singleService.getProduct($scope);
}])

app.controller('addProductController', ['$scope', 'addProductService', 'manageCategoriesService', function ($scope, addService, categoriesService) {
    $scope.message = "Thêm sản phẩm";

    $scope.listCategories = {
        CID: "",
        CNAME: "",
        DNAME: ""
    };
    categoriesService.getCategories($scope);
    
    addService.addProduct($scope);
}])

app.directive("ngFileSelect", function () {
    return {
        link: function ($scope, el) {
            el.on('click', function () {
                this.value = '';
            });

            el.bind("change", function (e) {
                $scope.file = (e.srcElement || e.target).files[0];
                var allowed = ["jpeg", "png", "gif", "jpg"];
                var found = false;
                var img;
                img = new Image();
                allowed.forEach(function (extension) {
                    if ($scope.file.type.match('image/' + extension)) {
                        found = true;
                    }
                });

                if (!found) {
                    alert('file type should be .jpeg, .png, .jpg, .gif');
                    return;
                }

                img.onload = function () {
                    var dimension = $scope.selectedImageOption.split(" ");
                    if (dimension[0] == this.width && dimension[2] == this.height) {
                        allowed.forEach(function (extension) {
                            if ($scope.file.type.match('image/' + extension)) {
                                found = true;
                            }
                        });

                        if (found) {
                            if ($scope.file.size <= 1048576) {
                                $scope.getFile();
                            } else {
                                alert('file size should not be grater then 1 mb.');
                            }
                        } else {
                            alert('file type should be .jpeg, .png, .jpg, .gif');
                        }
                    } else {
                        alert('selected image dimension is not equal to size drop down.');
                    }
                };
            });
        }
    };
});

app.service('addProductService', ['$http', function ($http) {
    this.addProduct = function ($scope) {
        $scope.submit = function () {
            if ($scope.TenSP && $scope.TenSP && $scope.LoaiSP && $scope.GiaSP && $scope.MuiVi && $scope.TinhTrang && $scope.MoTa) {
                var r = confirm("Bạn có thật sự muốn thêm?");
                if (r == true) {
                        var product = {
                            PID: 0,
                            PNAME: $scope.TenSP,
                            PCOST: $scope.GiaSP,
                            PSAVOUR: $scope.MuiVi,
                            PSTATUS: $scope.TinhTrang,
                            PDESCRIPTION: $scope.MoTa,
                            PIMG: "/Images/" + $scope.file.name,
                            PVIEW: 0,
                            CNAME: $scope.LoaiSP.CNAME,
                            BILLDETAIL: [],
                            CATEGORIES: null,
                            COMMENTS: []
                        };
                        var formData = new FormData();
                        formData.append("imageUploadForm", $scope.file);

                        $.ajax({
                            type: "POST",
                            url: '/Admin/Upload',
                            data: formData,
                            dataType: 'json',
                            contentType: false,
                            processData: false
                            
                        });
                        return $http({
                            method: "POST",
                            url: "http://ltdtwebapi.apphb.com/api/product/",
                            data: product,
                            headers: { 'Content-Type': 'application/json' }
                        }).success(function (data) {
                            console.log(data);
                            alert("Thêm thành công!");
                        }).error(function (data) {
                            console.log(data);
                            alert("Thêm thất bại !");
                        });;
                }
            } else {
                alert("Bạn chưa nhập đủ thông tin!");
            }
        }
    };
}]);

app.controller('updateProductController', ['$scope', 'updateProductService', 'manageCategoriesService', 'detailProductService', '$location', '$routeParams', function ($scope, updateService, categoriesService, singleService, $location, $routeParams) {
    $scope.message = "Cập nhật sản phẩm";

    $scope.listCategories = {
        CID: "",
        CNAME: "",
        DNAME: ""
    };
    categoriesService.getCategories($scope);

    $scope.product = {
        PID: "",
        PNAME: "",
        PCOST: "",
        PSAVOUR: "",
        PSTATUS: "",
        PDESCRIPTION: "",
        PIMG: "",
        PVIEW: "",
        CNAME: ""
    };

    $scope.PID = $routeParams.PID;
    singleService.getProduct($scope);
    singleService.setProduct($scope);

    updateService.updateProduct($scope, $location);
}])

app.service('detailProductService', ['$http', function ($http) {
    this.getProduct = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/product/" + $scope.PID,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.product = data;

            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    };

    this.setProduct = function ($scope) {
        $scope.TenSP = $scope.product.PNAME;
        $scope.LoaiSP = $scope.product.CNAME;
        $scope.GiaSP = $scope.product.PCOST;
        $scope.MuiVi = $scope.product.PSAVOUR;
        $scope.file = $scope.product.PIMG;
        $scope.TinhTrang = $scope.product.PSTATUS;
        $scope.MoTa = $scope.product.PDESCRIPTION;
    }
}]);

app.service('updateProductService', ['$http', function ($http) {
    this.updateProduct = function ($scope, $location) {
        $scope.submit = function () {
            if ($scope.TenSP && $scope.TenSP && $scope.LoaiSP && $scope.GiaSP && $scope.MuiVi && $scope.TinhTrang && $scope.MoTa) {
                var r = confirm("Bạn có thật sự muốn sửa?");
                if (r == true) {
                    var product = {
                        PID: $scope.PID,
                        PNAME: $scope.TenSP,
                        PCOST: $scope.GiaSP,
                        PSAVOUR: $scope.MuiVi,
                        PSTATUS: $scope.TinhTrang,
                        PDESCRIPTION: $scope.MoTa,
                        PIMG: "/Images/" + $scope.file.name,
                        PVIEW: $scope.product.PVIEW,
                        CNAME: $scope.LoaiSP.CNAME
                    };
                    var formData = new FormData();
                    formData.append("imageUploadForm", $scope.file);

                    $.ajax({
                        type: "POST",
                        url: '/Admin/Upload',
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        processData: false
               
                    });
                    return $http({
                        method: "PUT",
                        url: "http://ltdtwebapi.apphb.com/api/product/" + $scope.PID,
                        data: product,
                        headers: { 'Content-Type': 'application/json' }
                    }).success(function (data) {
                        console.log(data);
                        alert("Sửa thành công!");
                        $location.path = "/manageproducts"
                    }).error(function (data) {
                        console.log(data);
                        alert("SỬa thất bại!");
                    });
                }
            } else {
                alert("Bạn chưa nhập đủ thông tin!");
            }
        };
    }
}]);