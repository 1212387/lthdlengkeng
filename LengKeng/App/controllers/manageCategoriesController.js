﻿/// <reference path="manageCategoriesController.js" />
'use strict';

app.controller('manageCategoriesController', ['$scope', 'manageCategoriesService', 'manageDepartmentsService', '$location', '$http', function ($scope, categoriesService, departmentsService, $location, $http) {
    $scope.message = "Quản lý Category";

    $scope.listCategories = {
        CID: "",
        CNAME: "",
        DNAME:""
    };

    categoriesService.getCategories($scope);

    $scope.delcategory = function (CID) {
        var r = confirm("Bạn có thật sự muốn xóa?");
        if (r == true) {
            return $http({
                method: "DELETE",
                url: "http://ltdtwebapi.apphb.com/api/Catelogies/" + CID,
                headers: { 'Content-Type': 'application/json' }
            }).success(function (data) {
                console.log(data);
                alert("Xóa thành công!");
                $location.path('/managecategories');
            }).error(function (data) {
                console.log(data);
                alert("Xóa thất bại!");
            });
        }
    };
}])

app.service('manageCategoriesService', ['$http', function ($http) {
    this.getCategories = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/categories",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listCategories = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('addCategoryController', ['$scope', 'addCategoryService', 'manageDepartmentsService', function ($scope, addService, departmentsService) {
    $scope.message = "Thêm Category";

    $scope.listDepartments = {
        CATEGORIES: "",
        DID: "",
        DNAME: ""
    };
    departmentsService.getDepartments($scope);
    $scope.TenDepartment = $scope.listDepartments[0];

    addService.addCategory($scope);
}])

app.service('addCategoryService', ['$http', function ($http) {
    this.addCategory = function ($scope) {
        $scope.submit = function () {
            if ($scope.TenCategory && $scope.TenDepartment) {
                var r = confirm("Bạn có thật sự muốn thêm?");
                if (r == true) {
                    var category = {
                        CID: 0,
                        CNAME: $scope.TenCategory,
                        DNAME: $scope.TenDepartment.DNAME
                    };

                    return $http({
                        method: "POST",
                        url: "http://ltdtwebapi.apphb.com/api/categories",
                        data: category,
                        headers: { 'Content-Type': 'application/json' }
                    }).success(function (data) {
                        console.log(data);
                        alert("Thêm thành công!");
                    }).error(function (data) {
                        console.log(data);
                        alert("Thêm thất bại!");
                    });;
                }
            }
            else {
                alert("Bạn chưa nhập đủ thông tin!");
            }
        }
    };
}]);

app.controller('updateCategoryController', ['$scope', 'updateCategoryService', 'manageDepartmentsService', 'detailCategoryService', '$routeParams', '$location', function ($scope, updateService, departmentsService, singleService, $routeParams, $location) {
    $scope.message = "Cập nhật Category";

    $scope.listDepartments = {       
        DID: "",
        DNAME: "",
        CATEGORIES: ""
    };
    departmentsService.getDepartments($scope);

    $scope.CID = $routeParams.CID;
    singleService.getCategory($scope);

    updateService.updateCategory($scope, $location);
}])

app.service('detailCategoryService', ['$http', function ($http) {
    this.getCategory = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/categories/" + $scope.CID,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            var category = {
                CID: "",
                CNAME: "",
                DNAME: ""
            };

            category = data;
            $scope.TenCategory = category.CNAME;         

            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.service('updateCategoryService', ['$http', function ($http) {
    this.updateCategory = function ($scope, $location) {
        $scope.submit = function () {
            if ($scope.TenCategory && $scope.TenDepartment) {
                var r = confirm("Bạn có thật sự muốn sửa?");
                if (r == true) {
                    var category = {
                        CID: $scope.CID,
                        CNAME: $scope.TenCategory,
                        DNAME: $scope.TenDepartment.DNAME
                    };

                    return $http({
                        method: "PUT",
                        url: "http://ltdtwebapi.apphb.com/api/categories/" + $scope.CID,
                        data: category,
                        headers: { 'Content-Type': 'application/json' }
                    }).success(function (data) {
                        console.log(data);
                        alert("Sửa thành công!");
                        $location.path = "/managecategories"
                    }).error(function (data) {
                        console.log(data);
                        alert("Sửa thất bại!");
                    });;
                }
            } else {
                alert("Bạn chưa nhập đủ thông tin!");
            }
        }
    };
}]);