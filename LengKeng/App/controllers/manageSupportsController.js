﻿'use strict';

app.controller('manageSupportsController', ['$scope', 'manageSupportsService', function ($scope, supportsService) {
    $scope.message = "Quản lý phản hồi";

    $scope.listSupports = {
        SID: "",
        STITEL: "",
        SCONTENT: "",
        SDATECREATE: "",
        USERNAME: "",
        ACCOUNTS: ""
    };

    supportsService.getSupports($scope);
}])

app.service('manageSupportsService', ['$http', function ($http) {
    this.getSupports = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Supports",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listSupports = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('detailSupportController', ['$scope', 'detailSupportService', '$routeParams', function ($scope, singleService, $routeParams) {
    $scope.message = "Chi tiết phản hồi";

    $scope.support = {
        SID: "",
        STITEL: "",
        SCONTENT: "",
        SDATECREATE: "",
        USERNAME: "",
        ACCOUNTS: ""
    };

    $scope.SID = $routeParams.SID;

    singleService.getSupport($scope);
}])

app.service('detailSupportService', ['$http', function ($http) {
    this.getSupport = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Supports/" + $scope.SID,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.support = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    };
}]);