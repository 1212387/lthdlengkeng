﻿'use strict';

app.controller('manageBillsController', ['$scope', 'manageBillsService', function ($scope, billsService) {
    $scope.message = "Quản lý đơn đặt hàng";

    $scope.listBills = {
        BID: "",
        BDATECREATE: "",
        BTOTAL: "",
        BSTATUS: "",
        USERNAME: "",
        ACCOUNTS: "",
        BILLDETAIL: ""
    };

    billsService.getBills($scope);
}])

app.service('manageBillsService', ['$http', function ($http) {
    this.getBills = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Bills",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listBills = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('detailBillController', ['$scope', 'detailBillService', '$routeParams', function ($scope, singleService, $routeParams) {
    $scope.message = "Chi tiết hóa đơn";

    $scope.bill = {
        DETAILB_ID: "",
        IDPRODUCT: "",
        BID: "",
        AMOUNT: "",
        BILL: "",
        PRODUCTS: {
            PID: "",
            PNAME: "",
            PCOST: "",
            PSAVOUR: "",
            PSTATUS: "",
            PDESCRIPTION: "",
            PIMG: "",
            PVIEW: "",
            IDCATELOGY: "",
            BILLDETAIL: "",
            CATELOGIES: "",
            COMMENTS: ""
        }
    };

    $scope.BID = $routeParams.BID;

    singleService.getBill($scope);
}])

app.service('detailBillService', ['$http', function ($http) {
    this.getBill = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/getDetailBillByID/" + $scope.BID,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.bill = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);