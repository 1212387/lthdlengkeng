﻿'use strict';

app.controller('manageDepartmentsController', ['$scope', 'manageDepartmentsService', '$http', '$location', function ($scope, departmentsService, $http, $location) {
    $scope.message = "Quản lý Department";

    $scope.listDepartments = {
        DID: "",
        DNAME: "",
        CATEGORIES: ""    
    };

    departmentsService.getDepartments($scope);

    $scope.deldepartment = function (DID) {
        var r = confirm("Bạn có thật sự muốn xóa?");
        if (r == true) {
            return $http({
                method: "DELETE",
                url: "http://ltdtwebapi.apphb.com/api/Departments/" + DID,
                headers: { 'Content-Type': 'application/json' }
            }).success(function (data) {
                console.log(data);
                alert("Xóa thành công!");
                $location.path('/managedepartments');
            }).error(function (data) {
                console.log(data);
                alert("Xóa thất bại!");
            });
        }
    };
}])

app.service('manageDepartmentsService', ['$http', function ($http) {
    this.getDepartments = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Departments",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listDepartments = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };

    
}]);

app.controller('addDepartmentController', ['$scope', 'addDepartmentService', function ($scope, addService) {
    $scope.message = "Thêm Department";

    addService.addDepartment($scope);
}])

app.service('addDepartmentService', ['$http', function ($http) {
    this.addDepartment = function ($scope) {
        $scope.submit = function () {
            if ($scope.TenDepartment) {
                var department = {
                    DID: "0",
                    DNAME: $scope.TenDepartment
                };

                return $http({
                    method: "POST",
                    url: "http://ltdtwebapi.apphb.com/api/Departments",
                    data: department,
                    headers: { 'Content-Type': 'application/json' }
                }).success(function (data) {
                    console.log(data);
                    alert("Thêm thành công!");
                    $location.path('/managedepartments');
                }).error(function (data) {
                    console.log(data);
                    alert("Thêm thất bại!");
                });;
            }
        }
    };
}]);

app.controller('updateDepartmentController', ['$scope', 'updateDepartmentService', 'detailDepartmentService', '$routeParams', '$location', function ($scope, updateService, singleService, $routeParams, $location) {
    $scope.message = "Cập nhật Department";

    $scope.DID = $routeParams.DID;
    singleService.getDepartment($scope);

    updateService.updateDepartment($scope, $location);
}])

app.service('detailDepartmentService', ['$http', function ($http) {
    this.getDepartment = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Departments/" + $scope.DID,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            var Department = {
                DID: "",
                DNAME: "",
                CATEGORIES: ""
            };

            Department = data;
            $scope.TenDepartment = Department.DNAME;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.service('updateDepartmentService', ['$http', function ($http) {
    this.updateDepartment = function ($scope, $location) {
        $scope.submit = function () {
            var r = confirm("Bạn có thật sự muốn sửa?");
            if (r == true) {
                if ($scope.TenDepartment) {
                    var department = {
                        DID: $scope.DID,
                        DNAME: $scope.TenDepartment
                    };

                    return $http({
                        method: "PUT",
                        url: "http://ltdtwebapi.apphb.com/api/Departments/" + $scope.DID,
                        data: department,
                        headers: { 'Content-Type': 'application/json' }
                    }).success(function (data) {
                        console.log(data);
                        alert("Sửa thành công!");
                        $location.path('/managedepartments');
                    }).error(function (data) {
                        console.log(data);
                        alert("Sửa thất bại!");
                    });;
                }
            }
        }
    };
}]);