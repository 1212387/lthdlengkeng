﻿'use strict';

app.controller('loginController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {
    $scope.message = "Login";

    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message2 = "";

    $scope.login = function () {

        authService.login($scope.loginData).then(function (response) {
            alert("Đăng nhập thành công!");
            $location.path('/');

        },
         function (err) {
             $scope.message2 = err.error_description;
         });
    };

}]);