﻿'use strict';

app.controller('manageAccountsController', ['$scope', 'manageAccountsService', '$http', '$location', function ($scope, accountsService, $http, $location) {
    $scope.message = "Quản lý tài khoản";

    $scope.listAccounts = {
        USERNAME: "",
        PASSWORD: "",
        ADMIN: "",
        FULLNAME: "",
        BIRTHDAY: "",
        SEX: "",
        EMAIL: "",
        PHONENUMBER: "",
        ADDRESS: "",
        BILL: "",
        COMMENTS: "",
        SUPPORT: ""
    };

    accountsService.getAccounts($scope);

    $scope.delaccount = function (ID) {
        var r = confirm("Bạn có thật sự muốn xóa?");
        if (r == true) {
            return $http({
                method: "DELETE",
                url: "http://ltdtwebapi.apphb.com/api/Accounts/" + ID,
                headers: { 'Content-Type': 'application/json' }
            }).success(function (data) {
                console.log(data);
                alert("Xóa thành công!");
                $location.path('/manageaccounts');
            }).error(function (data) {
                console.log(data);
                alert("Xóa thất bại!");
            });
        }
    };
}])

app.service('manageAccountsService', ['$http', function ($http) {
    this.getAccounts = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/Accounts",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.listAccounts = data;
            console.log(data);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('registerAccountController', ['$scope', 'registerAccountService', function ($scope, addService) {
    $scope.message = "Đăng kí tài khoản";

    addService.addAccount($scope);
}])

app.service('registerAccountService', ['$http', function ($http) {
    this.addAccount = function ($scope) {
        $scope.submit = function () {
            var r = confirm("Bạn có thật sự muốn thêm?");
            if (r == true) {
                if ($scope.MatKhau != $scope.NhapLaiMatKhau) {
                    alert("Mật khẩu và nhập lại mật khẩu không trùng khớp!")
                } else {
                    if ($scope.TenDangNhap && $scope.MatKhau == $scope.NhapLaiMatKhau && $scope.Quyen && $scope.HoTen && $scope.GioiTinh && $scope.Email && $scope.SoDienThoai && $scope.NgaySinh && $scope.DiaChi) {
                        var admin;
                        if ($scope.Quyen == "Admin")
                            admin = 1;
                        else
                            admin = 0;

                        var account = {
                            USERNAME: $scope.TenDangNhap,
                            PASSWORD: $scope.MatKhau,
                            ADMIN: admin,
                            FULLNAME: $scope.HoTen,
                            BIRTHDAY: $scope.NgaySinh,
                            SEX: $scope.GioiTinh,
                            EMAIL: $scope.Email,
                            PHONENUMBER: $scope.SoDienThoai,
                            ADDRESS: $scope.DiaChi,
                            BILL: [],
                            COMMENTS: [],
                            SUPPORT: []
                        };

                        return $http({
                            method: "POST",
                            url: "http://ltdtwebapi.apphb.com/api/Accounts",
                            data: account,
                            headers: { 'Content-Type': 'application/json' }
                        }).success(function (data) {
                            console.log(data);
                            alert("Tạo thành công!");
                        }).error(function (data) {
                            console.log(data);
                            alert("Tạo thất bại!");
                        });;
                    } else {
                        alert("Bạn chưa nhập đủ thông tin!");
                    }
                }
            }
        }
    };
}]);

app.controller('detailAccountController', ['$scope', 'detailAccountService', '$routeParams', function ($scope, singleService, $routeParams) {
    $scope.message = "Thông tin tài khoản";

    $scope.account = {
        USERNAME: "",
        PASSWORD: "",
        ADMIN: "",
        FULLNAME: "",
        BIRTHDAY: "",
        SEX: "",
        EMAIL: "",
        PHONENUMBER: "",
        ADDRESS: "",
        BILL: "",
        COMMENTS: "",
        SUPPORT: ""
    };
    $scope.ROLE = "";

    $scope.USERNAME = $routeParams.USERNAME;

    singleService.getAccount($scope);
}])

app.service('detailAccountService', ['$http', function ($http) {
    this.getAccount = function ($scope) {
        return $http({
            method: "POST",
            url: "http://ltdtwebapi.apphb.com/api/getaccountby",
            data: { AID: $scope.USERNAME},
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.account = data;
            if ($scope.account.ADMIN == 1)
                $scope.ROLE = "Admin";
            else
                $scope.ROLE = "User";
            console.log(data);
        }).error(function (data) {
            console.log(data);
        });;
    };
}]);

app.controller('updateAccountController', ['$scope', 'updateAccountService', 'detailAccountService', '$routeParams', '$location', function ($scope, updateService, singleService, $routeParams, $location) {
    $scope.message = "Cập nhật tài khoản";

    $scope.ID = $routeParams.USERNAME;

    updateService.updateAccount($scope, $location);
}])

app.service('updateAccountService', ['$http', function ($http) {
    this.updateAccount = function ($scope, $location) {
        $scope.submit = function () {
            var r = confirm("Bạn có thật sự muốn sửa?");
            if (r == true) {
                if ($scope.Quyen) {
                    var admin;
                    if ($scope.Quyen == "Admin")
                        admin = 1;
                    else
                        admin = 0;

                    var account = {
                        ID: $scope.ID,
                        ADMIN: admin
                    };

                    return $http({
                        method: "PUT",
                        url: "http://ltdtwebapi.apphb.com/api/account",
                        data: account,
                        headers: { 'Content-Type': 'application/json' }
                    }).success(function (data) {
                        console.log(data);
                        alert("Sửa thành công!");
                        $location.path('/manageaccounts');
                    }).error(function (data) {
                        console.log(data);
                        alert("Sửa thất bại!");
                    });;
                }
            }
        }
    };
}]);