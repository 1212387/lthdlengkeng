﻿'use strict';
app.controller('homeController', ['$scope', 'manageService', function ($scope, manageService) {
    $scope.message = "Dashboard";

    $scope.num = {
        NUMACCCOUNT: "",
        NUMDEPARTMENT: "",
        NUMCATEGORY: "",
        NUMPRODUCT: "",
        NUMBILL: "",
        NUMSUPPORT: ""
    };
    manageService.getNum($scope);
}]);

app.service('manageService', ['$http', function ($http) {
    this.getNum = function ($scope) {
        return $http({
            method: "GET",
            url: "http://ltdtwebapi.apphb.com/api/num",
            headers: { 'Content-Type': 'application/json' }
        }).success(function (data) {
            $scope.num = data;
            console.log($scope.num);
        }).error(function (data) {
            console.log(data);
            //alert("error");
        });;
    };
}]);

app.controller('indexController', ['$scope', '$location', 'authService', function ($scope, $location, authService) {

    $scope.logOut = function () {
        authService.logOut();
        $location.path('/');
    }

    $scope.authentication = authService.authentication;

}]);