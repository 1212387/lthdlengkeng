﻿var app = angular.module('myApp', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar'])
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/App/views/dashboard.html',
            controller: 'homeController'
        })
        .when('/login', {
            templateUrl: '/App/views/login.html',
            controller: 'loginController'
        })
        .when('/manageaccounts', {
            templateUrl: '/App/views/manageAccounts.html',
            controller: 'manageAccountsController'
        })
        .when('/registeraccount', {
            templateUrl: '/App/views/registerAccount.html',
            controller: 'registerAccountController'
        })
        .when('/detailaccount/:USERNAME', {
            templateUrl: '/App/views/detailAccount.html',
            controller: 'detailAccountController'
        })
        .when('/updateaccount/:USERNAME', {
            templateUrl: '/App/views/updateAccount.html',
            controller: 'updateAccountController'
        })
        .when('/managecategories', {
            templateUrl: '/App/views/manageCategories.html',
            controller: 'manageCategoriesController'
        })
        .when('/addcategory', {
            templateUrl: '/App/views/addCategory.html',
            controller: 'addCategoryController'
        })
        .when('/updatecategory/:CID', {
            templateUrl: '/App/views/updateCategory.html',
            controller: 'updateCategoryController'
        })
        .when('/managedepartments', {
            templateUrl: '/App/views/manageDepartments.html',
            controller: 'manageDepartmentsController'
        })
        .when('/adddepartment', {
            templateUrl: '/App/views/addDepartment.html',
            controller: 'addDepartmentController'
        })
        .when('/updatedepartment/:DID', {
            templateUrl: '/App/views/updateDepartment.html',
            controller: 'updateDepartmentController'
        })
        .when('/manageproducts', {
            templateUrl: '/App/views/manageProducts.html',
            controller: 'manageProductsController'
        })
        .when('/detailproduct/:PID', {
            templateUrl: '/App/views/detailProduct.html',
            controller: 'detailProductController'
        })
        .when('/addproduct', {
            templateUrl: '/App/views/addProduct.html',
            controller: 'addProductController'
        })
        .when('/updateproduct/:PID', {
            templateUrl: '/App/views/updateProduct.html',
            controller: 'updateProductController'
        })
        .when('/managebills', {
            templateUrl: '/App/views/manageBills.html',
            controller: 'manageBillsController'
        })
        .when('/detailbill/:BID', {
            templateUrl: '/App/views/detailBill.html',
            controller: 'detailBillController'
        })
        .when('/managesupports', {
            templateUrl: '/App/views/manageSupports.html',
            controller: 'manageSupportsController'
        })
        .when('/detailsupport/:SID', {
            templateUrl: '/App/views/detailSupport.html',
            controller: 'detailSupportController'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);

var serviceBase = 'http://ltdtwebapi.apphb.com/';
app.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'ngAuthApp'
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);